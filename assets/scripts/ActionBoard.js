/*
Create By Lavi 10/6/2019
*/

const ActionType = cc.Enum({
    MOVE_TO: "TogMoveTo",
    MOVE_BY: "TogMoveBy",
    SCALE_TO: "TogScaleTo",
    SCALE_BY: "TogScaleBy",
    ROTATE_TO: "TogRotateTo",
    ROTATE_BY: "TogRotateBy",
    FADE_TO: "TogFadeTo",
    BLINK: "TogBlink",
    TINT_TO: "TogTintTo",
    TINT_BY: "TogTintBy",
});


cc.Class({
    extends: cc.Component,

    properties: {
        LabelTopLeft: cc.Label,
        LabelTopRight: cc.Label,
        LabelBottomLeft: cc.Label,
        LabelBottomRight: cc.Label,
    },

    start () {
        this.LabelTopLeft.string = this.LabelTopLeft.node.position;
        this.LabelTopRight.string = this.LabelTopRight.node.position;
        this.LabelBottomLeft.string = this.LabelBottomLeft.node.position;
        this.LabelBottomRight.string = this.LabelBottomRight.node.position;
    },

});
