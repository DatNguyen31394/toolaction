/*
Create By Lavi 10/6/2019
*/


cc.Class({
    extends: cc.Component,

    properties: {
        object: cc.Node,
        inputPositionX: cc.EditBox,
        inputPositionY: cc.EditBox,
        inputRotate: cc.EditBox,
        inputScale: cc.EditBox,
        inputSizeX: cc.EditBox,
        inputSizeY: cc.EditBox,
        inputOpacity: cc.EditBox,
        inputColorR: cc.EditBox,
        inputColorG: cc.EditBox,
        inputColorB: cc.EditBox,
        inputSkewX: cc.EditBox,
        inputSkewY: cc.EditBox,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.inputPositionX.string = this.object.x;
        this.inputPositionY.string = this.object.y;

        this.inputRotate.string = this.object.angle;
        this.inputScale.string = this.object.scale;
        this.inputSizeX.string = this.object.width;
        this.inputSizeY.string = this.object.height;
        this.inputOpacity.string = this.object.opacity;
        this.inputSkewX.string = this.object.skewX;
        this.inputSkewY.string = this.object.skewY;

        this.inputColorR.string = 255;
        this.inputColorG.string = 255;
        this.inputColorB.string = 255;
    },



    applyPropertie()
    {
        let colorR = Number(this.inputColorR.string);
        let colorG = Number(this.inputColorG.string);
        let colorB = Number(this.inputColorB.string);

        this.object.x = Number(this.inputPositionX.string);
        this.object.y = Number(this.inputPositionY.string);
        this.object.angle = Number(this.inputRotate.string);
        this.object.scale = Number(this.inputScale.string);
        this.object.width =  Number(this.inputSizeX.string);
        this.object.height = Number(this.inputSizeY.string);
        this.object.opacity = Number(this.inputOpacity.string);
        this.object.skewX = Number(this.inputSkewX.string);
        this.object.skewY = Number(this.inputSkewY.string);
        this.object.color = new cc.Color(colorR, colorG, colorB);
        cc.log(this.object);
    }
});
