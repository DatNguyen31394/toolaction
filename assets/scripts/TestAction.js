/*
Create By Lavi 10/6/2019
*/

const ActionType = cc.Enum({
    MOVE_TO: "TogMoveTo",
    MOVE_BY: "TogMoveBy",
    SCALE_TO: "TogScaleTo",
    SCALE_BY: "TogScaleBy",
    ROTATE_TO: "TogRotateTo",
    ROTATE_BY: "TogRotateBy",
    FADE_TO: "TogFadeTo",
    BLINK: "TogBlink",
    TINT_TO: "TogTintTo",
    TINT_BY: "TogTintBy",
});

const EaseType = cc.Enum({
    NONE: "TogNone",
    EASE_IN: "TogEaseIn",
    EASE_OUT: "TogEaseOut",
    EXPONENTIAL_IN: "TogExponentialIn",
    EXPONENTIAL_OUT: "TogExponentialOut",
    SINE_IN: "TogSineIn",
    SINE_OUT: "TogSineOut",
    ELASTIC_IN: "TogElasticIn",
    ELASTIC_OUT: "TogElasticOut",
    BOUNCE_IN: "TogBounceIn",
    BOUNCE_OUT: "TogBounceOut",
    BACK_IN: "TogBackIn",
    BACK_OUT: "TogBackOut",
});


cc.Class({
    extends: cc.Component,

    properties: {
        ObjectDemo: cc.Node,
        InputTimer: cc.EditBox,
        InputValue: cc.EditBox,
        _actionName: '',
        _easeName: '',
    },

    start () {
        this.InputValue.placeholder = 'Position (Ex: 200,200)';
        this._actionName = ActionType.MOVE_TO;
        this._easeName = EaseType.EASE_IN;
    },

    onResetData()
    {
        this._actionName = '';
        this._easeName = '';
        this.InputTimer.string = '';
        this.InputValue.string = '';
        this.ObjectDemo.position = cc.v2(-700,300);
        this.ObjectDemo.scale = 1;
        this.ObjectDemo.angle = 0;
        this.ObjectDemo.color = cc.Color.WHITE;
    },

    onRunAction()
    {
        let _timer = this.InputTimer.string;
        let _inputValue = this.InputValue.string;
        let arrValue = _inputValue.split(',');
        let _value = null;
        let _action = null;
        let _ease = null;

        if(arrValue.length == 2)
        {
            _value = cc.v2(arrValue[0],arrValue[1]);
        }
        else
        {
            _value = arrValue[0];
        }

        cc.log('VALUE :',_value);

        switch(this._actionName)
        {
            case ActionType.MOVE_TO:
                _action = cc.moveTo(_timer,_value);
                break;
            case ActionType.MOVE_BY:
                _action = cc.moveBy(_timer,_value);
                break;
            case ActionType.ROTATE_TO:
                _action = cc.rotateTo(_timer,_value);
                break;
            case ActionType.ROTATE_BY:
                _action = cc.rotateBy(_timer,_value);
                break;
            case ActionType.SCALE_TO:
                _action = cc.scaleTo(_timer,_value);
                break;
            case ActionType.SCALE_BY:
                _action = cc.scaleBy(_timer,_value);
                break;
            case ActionType.TINT_TO:
                break;
            case ActionType.TINT_BY:
                break;
            case ActionType.FADE_TO:
                _action = cc.fadeTo(_timer,_value);
                break;
            case ActionType.BLINK:
                _action = cc.blink(_timer,_value);
                break;
        }

        switch(this._easeName)
        {
            case EaseType.EASE_IN:
                _ease = cc.easeIn(1);
                break;
            case EaseType.EASE_OUT:
                _ease = cc.easeOut(1);
                break;
            case EaseType.EXPONENTIAL_IN:
                _ease = cc.easeExponentialIn(1);
                break;
            case EaseType.EXPONENTIAL_OUT:
                _ease = cc.easeExponentialOut(1);
                break;
            case EaseType.SINE_IN:
                _ease = cc.easeSineIn(1);
                break;
            case EaseType.SINE_OUT:
                _ease = cc.easeSineOut(1);
                break;
            case EaseType.ELASTIC_IN:
                _ease = cc.easeElasticIn(1);
                break;
            case EaseType.ELASTIC_OUT:
                _ease = cc.easeElasticOut(1);
                break;
            case EaseType.BOUNCE_IN:
                _ease = cc.easeBounceIn(1);
                break;
            case EaseType.BOUNCE_OUT:
                _ease = cc.easeBounceOut(1);
                break;
            case EaseType.BACK_IN:
                _ease = cc.easeBackIn(1);
                break;
            case EaseType.BACK_OUT:
                _ease = cc.easeBackOut(1);
                break;
        }

        if(this._easeName != EaseType.NONE)
        {
            cc.log('ease ');
            this.ObjectDemo.runAction(_action.easing(_ease));
            return;
        }
        cc.log('non ease ');
        this.ObjectDemo.runAction(_action);
    },

    onChangeAction(toggle)
    {
        cc.log(toggle.node.name);
        let valueText = '';

        switch(toggle.node.name)
        {
            case ActionType.MOVE_TO:
            case ActionType.MOVE_BY:
                valueText = 'Position (Ex: 200-200)';
                break;
            case ActionType.ROTATE_TO:
            case ActionType.ROTATE_BY:
                valueText = 'Angle (Ex: 50)';
                break;
            case ActionType.SCALE_TO:
            case ActionType.SCALE_BY:
                valueText = 'Scale (Ex: 0)';
                break;
            case ActionType.TINT_TO:
            case ActionType.TINT_BY:
                valueText = 'Color (Ex: 255-255-255)';
                break;
            case ActionType.FADE_TO:
                valueText= 'Opacity (Ex: 0.9)';
                break;
            case ActionType.BLINK:
                valueText = 'Blink Time (Ex: 5)';
                break;
        } 
        this.InputValue.placeholder = valueText;
        this._actionName = toggle.node.name;
    },

    onChangeEase(toggle)
    {
        cc.log('EASE ',toggle.node.name);
        this._easeName = toggle.node.name;
    }

});
